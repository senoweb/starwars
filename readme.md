Star Wars
==========

This is the repository for the Star Wars Project, developed with ❤️

----

Requirements:

- All the [Laravel requirements](https://laravel.com/docs/5.6/installation)
- PHP extensions: CURL, OPENSSL

----

change to root folder
```
$ cd project_folder
```

Install the dependencies
```
$ composer install
```

Copy .env.example to .env
```
$ cp .env.example .env
```

Generate the app key
```
$ php artisan key:generate
```

Grant framework storage permission
```
$ chgrp -R www-data storage bootstrap/cache
$ chmod -R ug+rwx storage bootstrap/cache
```