<?php

namespace App\Services;

use GuzzleHttp\Client;

abstract class Service
{
    protected $starwars;

    function __construct()
    {
        $this->starwars = new Client([
            'base_uri' => 'https://swapi.co/api/',
            'timeout'  => 10.0,
        ]);
    }
}
