<?php

namespace App\Services;

use GuzzleHttp\Exception\ClientException;

class StarWarsService extends Service
{
    protected $page;

    public function __construct()
    {
        parent::__construct();
    }

    public function getPlanets($page = 1) : array
    {
        $this->page = $page;

        return $this->getResource('planets');
    }

    public function getSpaceships($page = 1)
    {
        $this->page = $page;

        return $this->getResource('starships');
    }

    public function getVehicles($page = 1)
    {
        $this->page = $page;

        return $this->getResource('vehicles');
    }

    public function getPeople($page = 1)
    {
        $this->page = $page;

        return $this->getResource('people');
    }

    public function getFilms($page = 1)
    {
        $this->page = $page;

        return $this->getResource('films');
    }

    public function getSpecies($page = 1)
    {
        $this->page = $page;

        return $this->getResource('species');
    }

    protected function getResource(string $resource) : array
    {
        try {
            $request = $this->starwars->get(
                $resource,
                [
                    'query' => [
                        'format' => 'json',
                        'page' => $this->page
                    ]
                ]
            );

            $resources = json_decode((string)  $request->getBody());

            return [
                'total' =>  $resources->count ? (int) ceil($resources->count / 10) : 0,
                'page' => $this->page ?: 1,
                'next' => ($resources->next) ? $this->page + 1 : null,
                'previous' => ($resources->previous) ? $this->page - 1 : null,
                'items' => $resources->results ?: []
            ];
        } catch (ClientException $e) {
            abort(404);
        }
    }
}
