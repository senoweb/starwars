<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaginationRequest;
use App\Services\StarWarsService;

class StarWarsController extends Controller
{
    public function __construct(StarWarsService $service)
    {
      $this->service = $service;
    }

    public function index()
    {
        return view('home');
    }

    public function planets(PaginationRequest $request)
    {
        $planets = $this->service->getPlanets($request->page);

        return view('planets', compact('planets'));
    }

    public function spaceships(PaginationRequest $request)
    {
        $spaceships = $this->service->getSpaceships($request->page);

        return view('spaceships', compact('spaceships'));
    }

    public function vehicles(PaginationRequest $request)
    {
        $vehicles = $this->service->getVehicles($request->page);

        return view('vehicles', compact('vehicles'));
    }

    public function people(PaginationRequest $request)
    {
        $people = $this->service->getPeople($request->page);

        return view('people', compact('people'));
    }

    public function films(PaginationRequest $request)
    {
        $films = $this->service->getFilms($request->page);

        return view('films', compact('films'));
    }

    public function species(PaginationRequest $request)
    {
        $species = $this->service->getSpecies($request->page);

        return view('species', compact('species'));
    }
}
