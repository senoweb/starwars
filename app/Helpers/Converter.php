<?php

namespace App\Helpers;

  /**
   * @package NumbConverter
   * @author Precious Tom
   * @version  V1.0.0 [NumbConverter Assist in Shorting Abbreviating Large Numbers to Understanding and Equivalent Result]
   */
  class Converter
  {
    private $numb;

    static public function init($numb)
    {
      // Get The Value and Convert type to Int
      $int = (int) self::getValue($numb);
      // Get String Length
      $len = self::getStrLeng($int);
      // Check if Number Divisible by 3
      $stat = self::divBy3($len - 1);
      // Get Extention with value
      $output = self::zeroUnit(($len - 1), TRUE);
      // Contentanate Unit to Int
      $output = self::checkF2val($int) . $output;
      // Return Value
      return $output;
    }

    static public function checkF2val($int)
    {
      $o = $int;
      if ((strlen($int) > 3) && (strlen($int) <= 6)) {
        $o = $int / 1000;
      } else if ((strlen($int) > 6) && (strlen($int) <= 9)) {
        $o = $int / 1000000;
      } else if ((strlen($int) > 9) && (strlen($int) <= 12)) {
        $o = $int / 1000000000;
      } else if ((strlen($int) > 12) && (strlen($int) <= 15)) {
        $o = $int / 1000000000000;
      } else if ((strlen($int) > 15) && (strlen($int) <= 18)) {
        $o = $int / 1000000000000000;
      } else if ((strlen($int) > 18) && (strlen($int) <= 21)) {
        $o = $int / 1000000000000000000;
      } else if ((strlen($int) > 21) && (strlen($int) <= 24)) {
        $o = $int / 1000000000000000000000;
      }
      return $o;
    }

    static public function divBy3($number)
    {
      if ($number >= 3) {
        if ($number % 3 != 0) {
          $number += 3 - ($number % 3);
        }
      }
      return $number;
    }

    static public function getValue($numb)
    {
      return (int) $numb;
    }

    static public function getStrLeng($val)
    {
      $val = strlen($val);
      return $val;
    }

    static public function zeroUnit($index, $caps)
    {
      $zeroNunit = array(
         0 => '',   // Ten
         1 => '',   // Ten
         2 => '',  // Hundred
         3 => 'k',  4 => 'k',  5 => 'k',  // Thousand
         6 => 'm',  7 => 'm',  8 => 'm',  // Million
         9 => 'b', 10 => 'b', 11 => 'b',// Billion
        12 => 't', 13 => 't', 14 => 't', // Trillion
        15 => 'q', 16 => 'q', 17 => 'q', 18 => 'q', 19 => 'q', 20 => 'q', // Quadrillion
        21 => 's', 22 => 's', 23 => 's', 24 => 's', 25 => 's', 26 => 's', // Sextillion
        27 => 'o', 28 => 'o', 29 => 'o', // Octillion
        30 => 'n', 31 => 'n', 32 => 'n', // Nonillion
        33 => 'd', 34 => 'd', 35 => 'd', // Decillion
        36 => 'u', 37 => 'u', 37 => 'u' // Undecillion
      );
      $output = ($caps == TRUE) ? strtoupper($zeroNunit[$index]) : $zeroNunit[$index] ;
      return $output;
    }
  }