<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StarWarsController@index')->name('home');
Route::get('/planets', 'StarWarsController@planets')->name('planets');
Route::get('/spaceships', 'StarWarsController@spaceships')->name('spaceships');
Route::get('/vehicles', 'StarWarsController@vehicles')->name('vehicles');
Route::get('/people', 'StarWarsController@people')->name('people');
Route::get('/films', 'StarWarsController@films')->name('films');
Route::get('/species', 'StarWarsController@species')->name('species');
