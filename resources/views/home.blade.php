<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Star Wars</title>
        <!-- Boostrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #000;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>

        @yield('header')
    </head>
    <body>
        <div class="container">
            <div class="row content">
                <div class="col-md-12">
                    <div class="title">
                        Star Wars
                    </div>

                    <div class="links">
                        <a href="{{ route('planets') }}">Planetas</a>
                        <a href="{{ route('spaceships') }}">Naves espaciais</a>
                        <a href="{{ route('vehicles') }}">Veículos</a>
                        <a href="{{ route('people') }}">Personagens</a>
                        <a href="{{ route('films') }}">Filmes</a>
                        <a href="{{ route('species') }}">Espécies</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @yield('main')
                </div>
            </div>
          </div>
        </div>
        <!-- <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Star Wars
                </div>


            </div>
            <div class="content">

            </div>
        </div> -->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        @yield('scripts')
    </body>
</html>
