@extends('home')

@section('main')
  <div class="text-center">
    <h2>Naves espaciais</h2>
  </div>  
    <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Nome</th>
          <th>modelo</th>
          <th>fabricante</th>
          <th>Valor</th>
          <th>comprimento</th>
          <th>passageiros</th>
          <th>consumíveis</th>
          <th>Classe</th>
        </tr>
      </thead>
      <tbody>
        @foreach($spaceships['items'] as $spaceship)
          <tr>
            <td>{{ $spaceship->name }}</td>
            <td>{{ $spaceship->model }}</td>
            <td>{{ $spaceship->manufacturer }}</td>
            <td>{{ \Converter::init($spaceship->cost_in_credits) }}</td>
            <td>{{ $spaceship->length }}</td>
            <td>{{ \Converter::init($spaceship->passengers) }}</td>
            <td>{{ $spaceship->consumables }}</td>
            <td>{{ $spaceship->starship_class }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
    <nav class="text-center" aria-label="Page navigation">
      <ul class="pagination">
        <li class="disabled" >
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        @for ($i = 1; $i <= $spaceships['total']; $i++)
            <li class="{{ $i == $spaceships['page'] ? 'active' : ''}}"><a href="{{ route('spaceships', ['page' => $i]) }}">{{ $i }}</a></li>
        @endfor
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
@stop