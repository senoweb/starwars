@extends('home')

@section('main')
	<div class="text-center">
		<h2>Espécies</h2>
	</div>
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Nome</th>
					<th>classificação</th>
					<th>designação</th>
					<th>altura média</th>
					<th>Filmes</th>
				</tr>
			</thead>
			<tbody>
				@foreach($species['items'] as $specie)
					<tr>
						<td>{{ $specie->name }}</td>
						<td>{{ $specie->classification }}</td>
						<td>{{ $specie->designation }}</td>
						<td>{{ $specie->average_height }}</td>
						<td>{{ count($specie->films) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<nav class="text-center" aria-label="Page navigation">
			<ul class="pagination">
				<li class="disabled" >
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@for ($i = 1; $i <= $species['total']; $i++)
				    <li class="{{ $i == $species['page'] ? 'active' : ''}}"><a href="{{ route('species', ['page' => $i]) }}">{{ $i }}</a></li>
				@endfor
				<li>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
@stop