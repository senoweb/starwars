@extends('home')

@section('main')
	<div class="text-center">
		<h2>Planetas</h2>
	</div>	
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Populacao</th>
					<th>Clima</th>
					<th>Gravidade</th>
					<th>Solo</th>
					<th>água</th>
					<th>Rotacao</th>
					<th>Orbita</th>
					<th>Diâmetro</th>
					<th>Moradores</th>
					<th>Filmes</th>
				</tr>
			</thead>
			<tbody>
				@foreach($planets['items'] as $planet)
					<tr>
						<td>{{ $planet->name }}</td>
						<td>{{ \Converter::init($planet->population) }}</td>
						<td>{{ $planet->climate }}</td>
						<td>{{ $planet->gravity }}</td>
						<td>{{ $planet->terrain }}</td>
						<td>{{ $planet->surface_water }}</td>
						<td>{{ $planet->rotation_period }}</td>
						<td>{{ $planet->orbital_period }}</td>
						<td>{{ $planet->diameter }}</td>
						<td>{{ count($planet->residents) }}</td>
						<td>{{ count($planet->films) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<nav class="text-center" aria-label="Page navigation">
			<ul class="pagination">
				<li class="disabled" >
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@for ($i = 1; $i <= $planets['total']; $i++)
				    <li class="{{ $i == $planets['page'] ? 'active' : ''}}"><a href="{{ route('planets', ['page' => $i]) }}">{{ $i }}</a></li>
				@endfor
				<li>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
@stop