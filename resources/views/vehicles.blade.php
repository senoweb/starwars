@extends('home')

@section('main')
	<div class="text-center">
		<h2>Veículos</h2>
	</div>
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Nome</th>
					<th>modelo</th>
					<th>fabricante</th>
					<th>Valor</th>
					<th>comprimento</th>
					<th>passageiros</th>
					<th>consumíveis</th>
					<th>Classe</th>
				</tr>
			</thead>
			<tbody>
		        @foreach($vehicles['items'] as $vehicle)
					<tr>
						<td>{{ $vehicle->name }}</td>
						<td>{{ $vehicle->model }}</td>
						<td>{{ $vehicle->manufacturer }}</td>
						<td>{{ \Converter::init($vehicle->cost_in_credits) }}</td>
						<td>{{ $vehicle->length }}</td>
						<td>{{ \Converter::init($vehicle->passengers) }}</td>
						<td>{{ $vehicle->consumables }}</td>
						<td>{{ $vehicle->vehicle_class }}</td>
					</tr>
		        @endforeach
			</tbody>
		</table>
		<nav class="text-center" aria-label="Page navigation">
			<ul class="pagination">
				<li class="disabled" >
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@for ($i = 1; $i <= $vehicles['total']; $i++)
				    <li class="{{ $i == $vehicles['page'] ? 'active' : ''}}"><a href="{{ route('vehicles', ['page' => $i]) }}">{{ $i }}</a></li>
				@endfor
				<li>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
@stop