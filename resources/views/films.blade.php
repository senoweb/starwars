@extends('home')

@section('main')
	<div class="text-center">
		<h2>Filmes</h2>
	</div>
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>nome</th>
					<th>episode</th>
					<th>director</th>
					<th>producer</th>
					<th>release_date</th>
					<th>personagens</th>
					<th>planetas</th>
					<th>naves</th>
					<th>veiculos</th>
					<th>species</th>
				</tr>
			</thead>
			<tbody>
				@foreach($films['items'] as $film)
					<tr>
						<td>{{ $film->title }}</td>
						<td>{{ $film->episode_id }}</td>
						<td>{{ $film->director }}</td>
						<td>{{ $film->producer }}</td>
						<td>{{ \Carbon\Carbon::parse($film->release_date)->format('d/m/Y') }}</td>
						<td>{{ count($film->characters) }}</td>
						<td>{{ count($film->planets) }}</td>
						<td>{{ count($film->starships) }}</td>
						<td>{{ count($film->vehicles) }}</td>
						<td>{{ count($film->species) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<nav class="text-center" aria-label="Page navigation">
			<ul class="pagination">
				<li class="disabled" >
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@for ($i = 1; $i <= $films['total']; $i++)
				    <li class="{{ $i == $films['page'] ? 'active' : ''}}"><a href="{{ route('films', ['page' => $i]) }}">{{ $i }}</a></li>
				@endfor
				<li>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
@stop