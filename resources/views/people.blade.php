@extends('home')

@section('main')
	<div class="text-center">
		<h2>Personagens</h2>
	</div>
    <div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Altura</th>
					<th>massa</th>
					<th>Nascimento</th>
					<th>Sexo</th>
					<th>Filmes</th>
					<th>veiculos</th>
					<th>naves</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($people['items'] as $person)
					<tr>
						<td>{{ $person->name }}</td>
						<td>{{ $person->height }}</td>
						<td>{{ $person->mass }}</td>
						<td>{{ $person->birth_year }}</td>
						<td>{{ $person->gender }}</td>
						<td>{{ count($person->films) }}</td>
						<td>{{ count($person->vehicles) }}</td>
						<td>{{ count($person->starships) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<nav class="text-center" aria-label="Page navigation">
			<ul class="pagination">
				<li class="disabled" >
					<a href="#" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@for ($i = 1; $i <= $people['total']; $i++)
				    <li class="{{ $i == $people['page'] ? 'active' : ''}}"><a href="{{ route('people', ['page' => $i]) }}">{{ $i }}</a></li>
				@endfor
				<li>
					<a href="#" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
			</ul>
		</nav>
	</div>
@stop